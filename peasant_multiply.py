def peasant_multiply(x,y):
    if x==0:
        return 0
    elif x==1:
        return y
    else:
        x_half = x//2        
        tmp = peasant_multiply(x_half,y+y)
        if x_half*2==x:
            return tmp
        else:
            return tmp+y

if __name__=='__main__':
    print(peasant_multiply(4,3))
    print(peasant_multiply(5,3))
    print(peasant_multiply(5,-3))